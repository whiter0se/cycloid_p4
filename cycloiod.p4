struct CycloidRoutingTable {
  bit cyclic_neighbor;  // Index of cyclic neighbor (0 or 1)
  bit cubical_neighbor;  // Cubical neighbor index
  bit inside_leaf_set; // Predecessor (0) and successor (1) in local cycle
  bit outside_leaf_set; // Primary nodes in preceding (0) and succeeding (1) remote cycles
}

table cycloid_routing_table {
  key {
    inport: ingress_port;
    metadata {
      key: bit; // Key of the packet
    }
  }
  actions {
    output egress_port;
  }
  size: 1024;
  default_action = cycloid_forward;
}

table cycloid_next_hop {
  key {
    bit cyclic_index;
    bit cubical_index;
    bit target_key;
  }
  actions {
    output_port: egress_port;
  }
  size: 1024;
  default_action = drop;
}

// Function to calculate the direction (cubical or cyclic) for routing
bit get_direction(bit key, CycloidRoutingTable entry) {
  // Extract cyclic and cubical indices from key and routing table entry
  bit target_cyclic_index = key % 3;
  bit target_cubical_index = key / 3;
  return (target_cyclic_index != entry.inside_leaf_set) ? 0 : 1;
}

void cycloid_forward(inport, key, egress_port) {
  // Lookup routing table entry for current node
  var tb_entry <- cycloid_routing_table(ingress_port, key);

  // Calculate next hop based on key and routing table entry
  bit direction = get_direction(key, tb_entry);
  if (direction == 0) { // Cubical direction
    egress_port = compute_cubical_next_hop(tb_entry.cubical_neighbor, key);
  } else { // Cyclic direction
    egress_port = compute_cyclic_next_hop(tb_entry.cyclic_neighbor[direction-1], key, tb_entry.inside_leaf_set);
  }
}

// Functions to compute next hop in cubical and cyclic directions (implementation not provided)
output compute_cubical_next_hop(bit cubical_neighbor, bit key) { ... }
output compute_cyclic_next_hop(bit cyclic_neighbor, bit key, bit inside_leaf_set) { ... }
