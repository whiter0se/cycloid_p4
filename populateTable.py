import json
from p4runtime_lib.switch import RuntimeAPI  # Import the necessary class
from p4runtime_lib.error_utils import printGrpcError
from p4runtime_lib.convert import encode_str_to_p4rt_bytes

def loadTopology(filename):
    with open(filename, 'r') as f:
        return json.load(f)

def generate_routing_tables(topology):
    routing_tables = {}

    for node_id, node_info in enumerate(topology['nodes']):
        routing_tables[node_id] = []  
        cyclic_id = node_info['cyclic_id'] 
        cubical_id = node_info['cubical_id'] 
        cyclic_neighbors = node_info['cyclic_neighbors']  
        cubical_neighbor = node_info['cubical_neighbor']  
        leaf_set = node_info['leaf_set'] 

        entry = (cyclic_id, cubical_id, cyclic_neighbors, cubical_neighbor, leaf_set[0:2], leaf_set[2:]) 
        routing_tables[node_id].append(entry) 

    return routing_tables

def populate_p4_tables(routing_tables):
    # Iterate through switches
    for switch_id, (switch_address, switch_port) in switches.items():
        runtime_api = RuntimeAPI(switch_address, switch_port)  

        # Get Bindings to P4Info and switch connection
        bmv2_sw = runtime_api.get_bmv2_switch()  
        p4info_helper = runtime_api.get_p4info_helper()  

        table_entries = routing_tables[switch_id] 

        # Update the P4 table
        for entry in table_entries:
            table_entry = p4info_helper.buildTableEntry(
                table_name="MyIngress.cycloid_routing_table",
                match_fields={
                    # TODO: Populate match fields based on your P4 table structure
                    "hdr.ipv4.dstAddr": (entry[0], 32) 
                },
                action_name="MyIngress.set_egress_port",  # Example action
                action_params={
                    # TODO: Populate action parameters based on your P4 table structure
                    "egress_port": entry[1]
                }
            )
            try:
                bmv2_sw.WriteTableEntry(table_entry)
                print("Installed entry on {}".format(switch_address))
            except Exception as e:
                print("Error", e)

# ... Main Script Logic (similar to before) ... 
topology = loadTopology("topology.json")
routing_tables = generate_routing_tables(topology)


